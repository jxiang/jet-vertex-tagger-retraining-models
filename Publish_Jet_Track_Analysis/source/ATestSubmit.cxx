#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include <EventLoop/Algorithm.h>
#include <SampleHandler/ToolsDiscovery.h>
/*
#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODMuon/MuonContainer.h>
#include <TSystem.h>
#include <TFile.h>
#include <EventLoop/Worker.h>
*/

void ATestSubmit (const std::string& submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
//  const char* inputFilePath = gSystem->ExpandPathName ("$ALRB_TutorialData/r9315/");
  const char* inputFilePath = gSystem->ExpandPathName ("/eos/user/j/jxiang/QT_sample/mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_JETM1.e3668_s3126_r10201_r10210_p3529/");

//  SH::ScanDir().filePattern("AOD.11182705._000001.pool.root.1").scan(sh,inputFilePath);
  //SH::ScanDir().filePattern("DAOD_JETM1.13910715._000001.pool.root.1").scan(sh,inputFilePath);
 // SH::ScanDir().sampleDepth(-1).filePattern("*.pool.root.1").scan(sh,inputFilePath);
//SH::ScanDir().filePattern("*.pool.root.1").scan(sh,inputFilePath);

 SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_JETM1.e3668_s3126_r10201_r10210_p3529");


// SH::scanRucio (sh, "mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.recon.AOD.e3569_s3126_r10201");
//SH::addGrid (sh, "mc16_13TeV:AOD.12911492._00001*.pool.root.1");

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  job.options()->setDouble (EL::Job::optMaxEvents, 10000000); // for testing purposes, limit to run over the first 500 events only!

  // add our algorithm to the job
  EL::AnaAlgorithmConfig config;


  config.setType ("MyxAODAnalysis");

  // set the name of the algorithm (this is the name use with
  // messages)
  config.setName ("AnalysisAlg");

//Test 5
  config.setProperty("nc_outputSampleName", "nc_outputSampleName" );
  // define an output and an ntuple associated to that output
  EL::OutputStream output  ("nc_outputSampleName");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("nc_outputSampleName");
  job.algsAdd (ntuple);








//config.setProperty( "ElectronPtCut", 30000.0 ).ignore();
//config.setProperty( "SampleName", "Zee" ).ignore();

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (config);





//config.setProperty("m_outputStreamName", "m_outputStreamName" );
//config.m_outputStreamName = "myOutput"; // give the name of the output to our algorithm  








  // make the driver we want to use:
  // this one works by running the algorithm directly:
 // EL::DirectDriver driver;
  EL::PrunDriver driver;
driver.options()->setString("nc_outputSampleName", "user.jxiang.test.%in:name[2]%.%in:name[6]%"); 
 // we can use other drivers to run things on the Grid, with PROOF, etc.

  // process the job using the driver
  driver.submit (job, submitDir);











}
