#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
inputFilePath = os.getenv( 'ALRB_TutorialData' ) + '/r9315/'
ROOT.SH.ScanDir().filePattern( 'AOD.11182705._000001.pool.root.1' ).scan( sh, inputFilePath )
print(sh)

#Test 3lines
chain = ROOT.TChain( "CollectionTree" )
chain.Add( "file://$ASG_TEST_FILE_MC" )
sh.add( ROOT.SH.makeFromTChain( "input", chain ) )



# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )

# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'MyxAODAnalysis/AnalysisAlg' )
config.ElectronPtCut = 30000.0
config.SampleName = 'Zee'
job.algsAdd( config )

#test 8lines
stream = ROOT.EL.OutputStream( "minixAOD", "xAOD" )
job.outputAdd( stream )
from AsgAnalysisAlgorithms.xAODWriterAlg import xAODWriterAlg
job.algsAdd( xAODWriterAlg( "xAODWriter",
                            OutputLevel = 1,
                            ItemList = [ "xAOD::EventInfo#EventInfo",
                                         "xAOD::EventAuxInfo#EventInfoAux.-mcChannelNumber" ] ) )






# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
