#include "AsgTools/AnaToolHandle.h"
#include <JetInterface/IJetUpdateJvt.h>
#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODMuon/MuonContainer.h>
#include <TSystem.h>
#include <TFile.h>
#include <EventLoop/Worker.h>
#include <TBranch.h>
#include <TTree.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoopAlgs/AlgSelect.h>
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventBaseContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetAttributes.h"
#include <algorithm>


#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

using namespace asg::msgUserCode;

MyxAODAnalysis::MyxAODAnalysis(const std::string &name,
                               ISvcLocator *pSvcLocator)
        : EL::AnaAlgorithm(name, pSvcLocator), m_grl("GoodRunsListSelectionTool/grl", this),
          m_jetCleaning("JetCleaningTool/JetCleaning", this), m_JERTool("JERTool", this) {
    declareProperty("m_outputStreamName", m_outputStreamName = "m_outputStreamName",
                    "change outputname m_outputStreamName");
    declareProperty("JVFCorrName", m_jvfCorrName = "JVFCorr");
    declareProperty("SumPtTrkName", m_sumPtTrkName = "SumPtTrkPt500");
    declareProperty("VertexContainer", m_verticesName = "PrimaryVertices");
    declareProperty("AssociatedTracks", m_assocTracksName);

}


StatusCode MyxAODAnalysis::initialize() {
/*
   // GRL
  const char* GRLFilePath = "$ALRB_TutorialData/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
  const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(fullGRLFilePath);
  ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
  ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK(m_grl.initialize());


  // initialize and configure the jet cleaning tool
  ANA_CHECK (m_jetCleaning.setProperty( "CutLevel", "LooseBad"));
  ANA_CHECK (m_jetCleaning.setProperty("DoUgly", false));
  ANA_CHECK (m_jetCleaning.initialize());


  // instantiate and initialize the JER (using default configurations)
  ANA_CHECK(m_JERTool.initialize());
*/


/*
  JetCalibrationTool_handle.setTypeAndName("IJetCalibrationTool/name");
 
   // ANA_CHECK( ASG_MAKE_ANA_TOOL(JetCalibrationTool_handle, IJetCalibrationTool) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("JetCollection",jetAlgo.Data()) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("ConfigFile",config.Data()) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("CalibSequence",calibSeq.Data()) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("CalibArea",calibArea.Data()) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("IsData",isData) );
    ANA_CHECK( JetCalibrationTool_handle.retrieve() );
   
*/





    ANA_MSG_INFO("check I1");
    TFile *ofile = wk()->getOutputFile(m_outputStreamName);
    ANA_MSG_INFO("check I2");
    if (!ofile) {
        ATH_MSG_FATAL("Couldn't access output file for stream \""
                              << m_outputStreamName << "\"");
        return StatusCode::FAILURE;
    }
    ANA_MSG_INFO("check I3");
    //ANA_CHECK(event->writeTo(ofile));
    m_tree = new TTree("tree", "tree");
    m_tree->SetDirectory(ofile);

    s_tree = new TTree("TreeS", "TreeS");
    s_tree->SetDirectory(ofile);

    b_tree = new TTree("TreeB", "TreeB");
    b_tree->SetDirectory(ofile);


    ANA_MSG_INFO("check I4");

    m_tree->Branch("EventNumber", &m_EventNumber);
    m_tree->Branch("number_of_truth_jet", &number_of_truth_jet);
    m_tree->Branch("number_of_rec_jet", &number_of_rec_jet);
    m_tree->Branch("number_of_HS_jet", &number_of_HS_jet);
    m_tree->Branch("number_of_PU_jet", &number_of_PU_jet);
    m_tree->Branch("number_of_Else_jet", &number_of_Else_jet);
    m_tree->Branch("correctVertex", &correctVertex);
    m_tree->Branch("vertexdistance", &vertexdistance);

//  s_tree->Branch ("HS_jvfcorr", &HS_jvfcorr);
//  s_tree->Branch ("HS_rpt", &HS_rpt);
    s_tree->Branch("jvfcorr", &jvfcorr);
    s_tree->Branch("rpt", &rpt);
    s_tree->Branch("jetinfo", &jetinfo, "jetinfo[9]/F");
    s_tree->Branch("jettrackinfo", &jettrackinfo);
    s_tree->Branch("track_num_size", &track_num_size);
    s_tree->Branch("track_num_count", &track_num_count);
    s_tree->Branch("track_num_primary", &track_num_primary);
    s_tree->Branch("m_", &m_);
    s_tree->Branch("pt_", &pt_);
    s_tree->Branch("eta_", &eta_);
    s_tree->Branch("phi_", &phi_);
    s_tree->Branch("corrJVF_", &corrJVF_);
    s_tree->Branch("RpT_", &RpT_);
    s_tree->Branch("JVF_", &JVF_);
    s_tree->Branch("PV_Sum_PT_", &PV_Sum_PT_);
    s_tree->Branch("OTHER_SUM_PT_", &OTHER_SUM_PT_);
    s_tree->Branch("Num_Other_Track_", &Num_Other_Track_);
    s_tree->Branch("EV_num_", &EV_num_);


//  b_tree->Branch ("PU_jvfcorr", &PU_jvfcorr);
//  b_tree->Branch ("PU_rpt", &PU_rpt);
    b_tree->Branch("jvfcorr", &jvfcorr);
    b_tree->Branch("rpt", &rpt);
    b_tree->Branch("jetinfo", &jetinfo, "jetinfo[9]/F");
    b_tree->Branch("jettrackinfo", &jettrackinfo);
    b_tree->Branch("track_num_size", &track_num_size);
    b_tree->Branch("track_num_count", &track_num_count);
    b_tree->Branch("track_num_primary", &track_num_primary);
    b_tree->Branch("m_", &m_);
    b_tree->Branch("pt_", &pt_);
    b_tree->Branch("eta_", &eta_);
    b_tree->Branch("phi_", &phi_);
    b_tree->Branch("corrJVF_", &corrJVF_);
    b_tree->Branch("RpT_", &RpT_);
    b_tree->Branch("JVF_", &JVF_);
    b_tree->Branch("PV_Sum_PT_", &PV_Sum_PT_);
    b_tree->Branch("OTHER_SUM_PT_", &OTHER_SUM_PT_);
    b_tree->Branch("Num_Other_Track_", &Num_Other_Track_);
    b_tree->Branch("EV_num_", &EV_num_);



/*  
  m_tree->Branch ("AntiKt4EMTopoJets_m", &AntiKt4EMTopoJets_m);
  m_tree->Branch ("AntiKt4EMTopoJets_pt", &AntiKt4EMTopoJets_pt);
  m_tree->Branch ("AntiKt4EMTopoJets_eta", &AntiKt4EMTopoJets_eta);
  m_tree->Branch ("AntiKt4EMTopoJets_phi", &AntiKt4EMTopoJets_phi);
  m_tree->Branch ("AntiKt4EMTopoJets_jvfcorr", &AntiKt4EMTopoJets_jvfcorr);
  m_tree->Branch ("AntiKt4EMTopoJets_rpt", &AntiKt4EMTopoJets_rpt);
*/
    ANA_MSG_INFO("check I5");
    wk()->addOutput(m_tree);
    wk()->addOutput(s_tree);
    wk()->addOutput(b_tree);

    ANA_MSG_INFO("check I6");



    return StatusCode::SUCCESS;

}


StatusCode MyxAODAnalysis::execute() {


//ANA_MSG_INFO ("in execute");


// Get Basic Info


//Creating and saving ntuples with trees


//calibration
    xAOD::TStore store;

    const xAOD::EventInfo *eventInfo = 0;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
    // fill the branches of our trees
    m_EventNumber = eventInfo->eventNumber();
    jetinfo[0] = m_EventNumber;
//ANA_MSG_INFO ("check 1");

    number_of_HS_jet = 0;
    number_of_Else_jet = 0;
    number_of_PU_jet = 0;
    correctVertex = 0;

    HS_jvfcorr = -9;
    HS_rpt = -9;
    PU_jvfcorr = -9;
    PU_rpt = -9;




///////////////////////////////////////////

//ANA_MSG_INFO ("check 2");






////////////////////AOD File do not have truthjet   210-265



    // get jet container of interest
    const xAOD::JetContainer *TruthJets = 0;
    ANA_CHECK(evtStore()->retrieve(TruthJets, "AntiKt4TruthJets"));
    // ANA_MSG_INFO ("execute(): number of EMTopojets = " << EMTopojets->size());


    number_of_truth_jet = TruthJets->size();

    float **TruthJetMomentum;
    TruthJetMomentum = new float *[number_of_truth_jet];

    for (int i = 0; i < number_of_truth_jet; i++) {
        TruthJetMomentum[i] = new float[4];
        memset(TruthJetMomentum[i], 0, 4 * sizeof(float));
    }


    int Truthloops = 0;





    // loop over the jets in the container
    for (const xAOD::Jet *jet : *TruthJets) {

        TruthJets_m = jet->m();
        TruthJets_pt = jet->pt();
        TruthJets_eta = jet->eta();
        TruthJets_phi = jet->phi();

        TruthJetMomentum[Truthloops][0] = TruthJets_m;
        TruthJetMomentum[Truthloops][1] = TruthJets_pt;
        TruthJetMomentum[Truthloops][2] = TruthJets_eta;
        TruthJetMomentum[Truthloops][3] = TruthJets_phi;

        Truthloops++;


    } // end for loop over jets




//ANA_MSG_INFO ("check 3");








//////////////////////////////////////////////























    // get jet container of interest
    const xAOD::JetContainer *EMTopojets = 0;
    ANA_CHECK(evtStore()->retrieve(EMTopojets, "AntiKt4EMTopoJets"));
    // ANA_MSG_INFO ("execute(): number of EMTopojets = " << EMTopojets->size());


    number_of_rec_jet = EMTopojets->size();
    //ANA_MSG_INFO (number_of_rec_jet);

    //ANA_MSG_INFO ("check 3.5");

    //std::cout<<findHSVertex()->vertexType ()<<"HS"<<std::endl;

    /*
    const xAOD::Vertex* HSvertex = findHSVertex();
    if(!HSvertex)
    {ANA_MSG_INFO ("No HS Vertex");}
    
    */
    ////Check primary vertex


    /*
    const xAOD::TruthEventContainer *tec = 0;
    ANA_CHECK (evtStore()->retrieve(tec,"TruthEvents"));
    const xAOD::TruthEvent* evt = 0;
    //ANA_CHECK (evtStore()->retrieve(evt,"TruthEvents"));
    evt = tec->begin();   
    const xAOD::TruthVertex* TruthPriVertex0 = evt->truthVertex(0);
    const xAOD::TruthVertex* TruthPriVertexsg = evt->signalProcessVertex();
    vertexdistance = TruthPriVertexsg->z() - HSvertex->z();
    if(vertexdistance < 0.1 && vertexdistance > -0.1)
    {
        correctVertex = 1;
    }
    */

    /*
    ANA_MSG_INFO ("check 3.6");
    const xAOD::TruthEventContainer *tec = 0;
    ANA_CHECK(evtStore()->retrieve(tec,"TruthEvents"));
    if(fabs(HSvertex->z()-(*tec->begin())->signalProcessVertex()->z())<0.1)
    {
        correctVertex = 1;
        vertexdistance = fabs(HSvertex->z()-(*tec->begin())->signalProcessVertex()->z());
    }

    //std::cout<<std::endl<<"Primary Vertex: "<<HSvertex<< std::endl;

    */
    //ANA_MSG_INFO ("check 4");

    // loop over the jets in the container
    for (const xAOD::Jet *jet : *EMTopojets) {


        /*
        //Calibration
        xAOD::Jet * jet = 0;
        JetCalibrationTool_handle->calibratedCopy(*ijet,jet); //make a calibrated copy
        */


        //ANA_MSG_INFO ("check 4.5");




        track_num_size = 0;
        track_num_count = 0;
        track_num_primary = 0;

        corrJVF_ = -0.5;
        RpT_ = -0.5;
        JVF_ = -0.5;

        PV_Sum_PT_ = -0.5;
        OTHER_SUM_PT_ = -0.5;
        Num_Other_Track_ = -2;

        int n_PUTrk = 0;
        double pT_pV_sum = 0;
        double pT_Other_Sum = 0;


        jvfcorr = -9;
        rpt = -9;


        AntiKt4EMTopoJets_m = jet->m();
        AntiKt4EMTopoJets_pt = jet->pt();
        AntiKt4EMTopoJets_eta = jet->eta();
        AntiKt4EMTopoJets_phi = jet->phi();
        //AntiKt4EMTopoJets_jvfcorr = jet->getAttribute<float>(m_jvfCorrName);
        AntiKt4EMTopoJets_jvfcorr =  jet->auxdata<float>("JVFCorr");

        //new all track
        //std::vector<float> sumpttrkpt500 = jet->getAttribute<std::vector<float> >(m_sumPtTrkName);
        //AntiKt4EMTopoJets_rpt = sumpttrkpt500[HSvertex->index()]/jet->pt();

        if (AntiKt4EMTopoJets_rpt > 3) { AntiKt4EMTopoJets_rpt = -2; }

        //ANA_MSG_INFO ("check 4.6");

        jvfcorr = AntiKt4EMTopoJets_jvfcorr;
        rpt = AntiKt4EMTopoJets_rpt;
        EV_num_ = m_EventNumber;

        jetinfo[1] = AntiKt4EMTopoJets_m;
        jetinfo[2] = AntiKt4EMTopoJets_pt;
        jetinfo[3] = AntiKt4EMTopoJets_eta;
        jetinfo[4] = AntiKt4EMTopoJets_phi;

        jetinfo[5] = AntiKt4EMTopoJets_jvfcorr;
        jetinfo[6] = AntiKt4EMTopoJets_rpt;

        jetinfo[7] = correctVertex;
        jetinfo[8] = vertexdistance;
        //newjvt = h_updatejvt->updateJvt(*jet);
        newjvt = 0;

        jetinfo[9] = newjvt;

        m_ = AntiKt4EMTopoJets_m;
        pt_ = AntiKt4EMTopoJets_pt;
        eta_ = AntiKt4EMTopoJets_eta;
        phi_ = AntiKt4EMTopoJets_phi;

        jettrackinfo.clear();


        //test JVT



        //ANA_MSG_INFO ("check 4.7");



        //get track information

        std::vector<const xAOD::TrackParticle *> tracks;
        if (!jet->getAssociatedObjects(xAOD::JetAttribute::GhostTrack, tracks)) {
            ATH_MSG_WARNING("Associated tracks not found.");
            tracks.clear();
            jettrackinfo.push_back(0);
        } else {
            jettrackinfo.push_back(tracks.size());//jettrackinfo(0)
            //std::cout<<std::endl<<std::endl<<std::endl<< jettrackinfo[0]<<std::endl;

            track_num_size = jettrackinfo[0];


            float **AllTracks;
            AllTracks = new float *[tracks.size()];

            for (int i = 0; i < tracks.size(); i++) {
                AllTracks[i] = new float[7];
                memset(AllTracks[i], 0, 7 * sizeof(float));

            }

            int realtrack = 0;
            int arraysize = tracks.size();

            for (size_t iTrack = 0; iTrack < tracks.size(); ++iTrack) {
                const xAOD::TrackParticle *track = tracks.at(iTrack);

                if (track) {

                    //ANA_MSG_INFO ("check A1");
                    //ANA_MSG_INFO (track->pt());


                    AllTracks[realtrack][0] = track->m();
                    AllTracks[realtrack][1] = track->pt();
                    AllTracks[realtrack][2] = track->eta();
                    AllTracks[realtrack][3] = track->phi();
                    AllTracks[realtrack][4] = -1;  // vertex index(1->PV; 2->Other_V)
                    AllTracks[realtrack][5] = 0;  // vertex rank (track number)
                    AllTracks[realtrack][6] = 0;  // track number for a single vertex





                    //std::cout<<std::endl<<"Vertex"<<track->vertexLink()<< std::endl;
                    //std::cout<<std::endl<<"Vertex: "<<track->vertex ()<< std::endl;
                    if (track->vertex() != 0) {
                        //std::cout<<(track->vertex()->vertexType() == xAOD::VxType::PriVtx)<< std::endl;
                    }


                    if (track->vertex() != 0) {

                        if (track->vertex()->vertexType() == xAOD::VxType::PriVtx) {
                            track_num_primary++;
                            pT_pV_sum += track->pt();
                            AllTracks[realtrack][4] = 1;
                        } else {
                            n_PUTrk++;
                            pT_Other_Sum += track->pt();
                            AllTracks[realtrack][4] = 2;
                        }

                    }
                    else{
                        AllTracks[realtrack][4] = -1;

                    }


                    realtrack++;

                }


            }

            /*

            if(AllTracks[0])
            {
                std::cout<<AllTracks[0][4]<< std::endl;

                if(AllTracks[0][4]>-0.5 && AllTracks[0][4]<0.5)
                {
                    std::cout<<realtrack<< std::endl;
                }
                std::cout<< std::endl;

            }

             */




            //ANA_MSG_INFO ("check 5");

            //ANA_MSG_INFO (realtrack);
            track_num_count = realtrack;

            //std::sort(AllTracks,AllTracks + arraysize,cmp);
            //ANA_MSG_INFO ("check I11");

            //sort Alltracks
            jettrackinfo.push_back(realtrack);  //jettrackinfo(1)

            for (int t_info = 0; t_info < realtrack; t_info++) {  //jettrackinfo(2-6)
                int track_num = 0;
                track_num = 4 * t_info;


                jettrackinfo.push_back(AllTracks[t_info][0]);
                jettrackinfo.push_back(AllTracks[t_info][1]);
                jettrackinfo.push_back(AllTracks[t_info][2]);
                jettrackinfo.push_back(AllTracks[t_info][3]);
                jettrackinfo.push_back(AllTracks[t_info][4]);


            }


            corrJVF_ = pT_pV_sum / (pT_pV_sum + (pT_Other_Sum / (n_PUTrk * 0.5)));
            RpT_ = pT_pV_sum / pt_;
            jetinfo[6] = RpT_;
            
            JVF_ = pT_pV_sum / (pT_pV_sum + pT_Other_Sum);

            PV_Sum_PT_ = pT_pV_sum;
            OTHER_SUM_PT_ = pT_Other_Sum;
            Num_Other_Track_ = n_PUTrk;

            if (PV_Sum_PT_ > 150000){
                PV_Sum_PT_ = -10000;
            }
            if (OTHER_SUM_PT_ > 150000){
                OTHER_SUM_PT_ = -10000;
            }


            if (n_PUTrk == 0) {
                corrJVF_ = -2;
            }

            if (corrJVF_ > 2.1) {
                corrJVF_ = -1;
            }
            if (RpT_ > 2.1) {
                RpT_ = -1;
            }
            if (JVF_ > 2.1) {
                JVF_ = -1;
            }
            
            jetinfo[6] = RpT_;



            delete[] AllTracks;


        }  //end else





        //ANA_MSG_INFO ("check 6");





        // AOD no truthjet 504-505, 619


        /*
        jettrackinfo.clear();
        jettrackinfo.push_back(0);
        jettrackinfo.push_back(1);

        std::cout<<jettrackinfo.size()<< std::endl;
         */






        int HSexist = 0;
        int NAexist = 0;

        for (int i = 0; i < number_of_truth_jet; i++) {
            int testHSPU = checkHS(AntiKt4EMTopoJets_pt, AntiKt4EMTopoJets_eta, AntiKt4EMTopoJets_phi,
                                   TruthJetMomentum[i][1], TruthJetMomentum[i][2], TruthJetMomentum[i][3]);
            // int testHSPU = 1;

            if (testHSPU == 1) {
                HSexist = 1;


            }

            if (testHSPU == 0) {
                NAexist = 1;
            }


        }

        //std::cout<<"("<<HSexist<<" , "<<NAexist<< ")"<<std::endl;
        //std::cout<<jetinfo[2]<<std::endl;

        //A test cut
        if (jetinfo[2] < 120000 && jetinfo[2] > 20000 && jetinfo[3] > -2.5 && jetinfo[3] < 2.5)
            // if(50000 > 40000)

        {  //554-579

            //std::cout<<jetinfo[2]<<std::endl;


            if (HSexist == 1) {
                number_of_HS_jet++;
                //    HS_jvfcorr = AntiKt4EMTopoJets_jvfcorr;
                //   HS_rpt = AntiKt4EMTopoJets_rpt;
                //ANA_MSG_INFO (number_of_HS_jet);
                s_tree->Fill();

            }
            if (HSexist == 0 && NAexist == 0) {
                number_of_PU_jet++;
                //    PU_jvfcorr = AntiKt4EMTopoJets_jvfcorr;
                //   PU_rpt = AntiKt4EMTopoJets_rpt;
                //ANA_MSG_INFO (number_of_PU_jet);
                b_tree->Fill();

            }
            if (HSexist == 0 && NAexist == 1) {
                number_of_Else_jet++;
                //ANA_MSG_INFO (number_of_Else_jet);


            }

        }  //554-579

        //delete jet;


        // ANA_MSG_INFO ("check 7");























    } // end for loop over jets




    //ANA_MSG_INFO ("check 7.5");



    m_tree->Fill();



















    //ANA_MSG_INFO ("check 8");



//ANA_MSG_INFO ("check 2");
    // testbr1->Fill();
//ANA_MSG_INFO ("check 3");

    delete[] TruthJetMomentum;

    //ANA_MSG_INFO ("check 9");


    return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis::finalize() {


    ANA_MSG_INFO("in finalize");


    return StatusCode::SUCCESS;
}


const xAOD::Vertex *MyxAODAnalysis::findHSVertex() const {

    // Get the vertices container
    const xAOD::VertexContainer *vertices = NULL;
    if (evtStore()->retrieve(vertices, m_verticesName).isFailure()) {
        ATH_MSG_ERROR("Could not retrieve the VertexContainer from evtStore: " << m_verticesName);
        return nullptr;
    }
    ATH_MSG_DEBUG("Successfully retrieved VertexContainer from evtStore: " << m_verticesName);

    if (vertices->size() == 0) {
        ATH_MSG_WARNING("There are no vertices in the container. Exiting");
        return nullptr;
    }

    for (size_t iVertex = 0; iVertex < vertices->size(); ++iVertex) {
        if (vertices->at(iVertex)->vertexType() == xAOD::VxType::PriVtx) {

            ATH_MSG_VERBOSE("JetVertexTaggerTool " << name() << " Found HS vertex at index: " << iVertex);
            return vertices->at(iVertex);
        }
    }
    ATH_MSG_VERBOSE("There is no vertex of type PriVx. Taking default vertex.");
    //return vertices->at(0);
    return nullptr;
}


int MyxAODAnalysis::checkHS(
        float Recpt,
        float Receta,
        float Recphi,
        float Truthpt,
        float Trutheta,
        float Truthphi
) {


    float dR = sqrt((Receta - Trutheta) * (Receta - Trutheta) + (Recphi - Truthphi) * (Recphi - Truthphi));

    // ANA_MSG_INFO ("Recpt = " << (Recpt) );
    // ANA_MSG_INFO ("Truthpt = " << (Truthpt) );

    if ((Truthpt > 10000) && dR < 0.3 && Recpt > 20000) {
        return 1;


    } else if (dR > 0.6 || Recpt < 20000) {
        return -1;


    } else {
        return 0;
    }

    ANA_MSG_INFO("Error");
    return -12;


}


bool MyxAODAnalysis::cmp(const float *a, const float *b) {
    return a[1] > b[1];
}















