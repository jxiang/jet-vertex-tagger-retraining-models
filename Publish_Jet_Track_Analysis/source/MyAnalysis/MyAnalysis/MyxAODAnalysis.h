#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>

#include <JetInterface/IJetSelector.h>
#include <JetResolution/IJERTool.h>

#include <TTree.h>
#include <EventLoop/Worker.h>


#include "TFile.h"
#include "TTreeFormula.h"


#include "JetCalibTools/IJetCalibrationTool.h"
#include "xAODJet/JetAttributes.h"
#include <algorithm>

#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

#include "AsgTools/AnaToolHandle.h"
#include <JetInterface/IJetUpdateJvt.h>




class MyxAODAnalysis : public EL::AnaAlgorithm 
{
public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);


  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
  
asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!

asg::AnaToolHandle<IJetUpdateJvt> h_updatejvt; //!

int m_numCleanEvents = 0; //!
int m_numAllEvents = 0; //!



  TString jetAlgo    = "AntiKt4EMTopo";  // Jet collection, for example AntiKt4EMTopo or AntiKt4LCTopo (see below)
  TString config     = "JES_data2017_2016_2015_Recommendation_Feb2018_rel21.config";  // Global config (see below)
  TString calibSeq  = "JetArea_Residual_EtaJES_GSC"; // Calibration sequence to apply (see below)
  TString calibArea = "00-04-81"; // Calibration Area tag (see below)
  bool isData          = "false"; // bool describing if the events are data or from simulation
  

  





asg::AnaToolHandle<IJetSelector> m_jetCleaning; //!
// JER
  asg::AnaToolHandle<IJERTool> m_JERTool; //! 

 // asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle; //! 
  
  const xAOD::Vertex* findHSVertex() const;
  int checkHS ( 
            float Recpt,
            float Receta,
            float Recphi,
            float Truthpt,
            float Trutheta,
            float Truthphi
            );
            
   static bool cmp (const float *a, const float *b);         


 

private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

   /// Electron pT cut
   double m_electronPtCut;
   /// Sample name
   std::string m_sampleName;
   


//Creating and saving ntuples with trees
  // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree
  std::string m_outputStreamName;
  TTree *m_tree; //!
  TTree *s_tree; //!
  TTree *b_tree; //!
  
  
  //int m_EventNumber; //!
  int AntiKt4EMTopoJets_size; //!
  float AntiKt4EMTopoJets_m; //!
  float AntiKt4EMTopoJets_pt; //!
  float AntiKt4EMTopoJets_eta; //!
  float AntiKt4EMTopoJets_phi; //!
  float AntiKt4EMTopoJets_jvfcorr; //!
  float AntiKt4EMTopoJets_rpt; //!
  
  float TruthJets_m; //!
  float TruthJets_pt; //!
  float TruthJets_eta; //!
  float TruthJets_phi; //!


  float m_; //!
  float pt_; //!
  float eta_; //!
  float phi_; //!



  float corrJVF_; //!
  float RpT_; //!
  float JVF_; //!
  int EV_num_; //!

  float PV_Sum_PT_; //!
  float OTHER_SUM_PT_; //!
  int Num_Other_Track_; //!
  
  int m_EventNumber; //!
  int number_of_truth_jet; //!
  int number_of_rec_jet; //!
  int number_of_HS_jet; //!
  int number_of_PU_jet; //!
  int number_of_Else_jet; //!
  int correctVertex; //!
  float vertexdistance; //!

  int track_num_size; //!
  int track_num_count; //!
  int track_num_primary; //!

  
  float HS_jvfcorr; //!
  float HS_rpt; //!
  
  float PU_jvfcorr; //!
  float PU_rpt; //!
  
  float jvfcorr; //!
  float rpt; //!  
  float jetinfo[10]; //!
  std::vector<float> jettrackinfo; //!
  float newjvt; //!

  
  
  
  std::string m_jvfCorrName; //!
  std::string m_sumPtTrkName; //!
  std::string m_verticesName; //!
  std::string m_assocTracksName; //!
  TBranch *testbr1; //!
//  TFile *ofile; //!
// TFile *ofile; //!  















};

#endif
