#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include <chrono>

using namespace TMVA;
using namespace std;
using namespace chrono;

bool sortcol(const vector<float> &v1, const vector<float> &v2) {
    return v1[0] > v2[0];
}

void return_current_time_and_date() {
    auto now = system_clock::now();
    auto in_time_t = system_clock::to_time_t(now);
    cout << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X") << endl;
    return;
}

float Get_AUC_Score(const vector <vector<float>> &ROC_Vector) {
    float AUC_Score = -1;
    int bin_num = ROC_Vector.size();
    float BR_sum = 0;

    for (auto point:ROC_Vector) {
        BR_sum += point[1];
        cout<<point[0]<<"\t"<<point[1]<<endl;
    }

    AUC_Score = BR_sum / (bin_num + 0.0000001);
    return AUC_Score;
}

tuple <vector<vector < float>>, int, float>

ROC_Points(const vector <vector<float>> &Check_ROC_S, const vector <vector<float>> &Check_ROC_B,
           const int &bin_number) {
    int ROC_bins = bin_number;
    if (bin_number == 0) {
        ROC_bins = 1000;
    }
    float AUC_score = -1;
    vector <vector<float>> ROC_Vector;
    int Sig_size = Check_ROC_S.size();
    int BK_size = Check_ROC_B.size();
    int cut_num = Sig_size / ROC_bins;
    for (int i = 0; i < bin_number; i++) {
        vector<float> ROC_Point;
        int BK_num = 0;
        float Sig_Effi = ((i + 0.0000001) / (bin_number + 0.0000001));
        float Current_JVT = Check_ROC_S[i * cut_num][0];
        for(int i =0;((Check_ROC_B[i][0]>Current_JVT)&&(i<BK_size));i++){
            BK_num+=1;
        }
        float BK_Rej = 1-((BK_num+ 0.0000001)/(BK_size+ 0.0000001));
        ROC_Point.push_back(Sig_Effi);
        ROC_Point.push_back(BK_Rej);
        ROC_Vector.push_back(ROC_Point);
    }
    AUC_score=Get_AUC_Score(ROC_Vector);
    tuple < vector < vector < float >> , int, float > Result_tuple = make_tuple(ROC_Vector, ROC_bins, AUC_score);
    return Result_tuple;
}

tuple <vector<vector < float>>, int, float> Cal_AUC(int Output_or_not=0) {

    auto start = system_clock::now();

    TChain *chain0 = new TChain("Evaluated_TreeS");
    chain0->Add("Checketapt.root");
    TChain *chain1 = new TChain("Evaluated_TreeB");
    chain1->Add("Checketapt.root");
    Float_t jetevaluated[5];

    chain0->SetBranchAddress("jetevaluated", &jetevaluated);
    chain1->SetBranchAddress("jetevaluated", &jetevaluated);


    Int_t nentries0 = (Int_t) chain0->GetEntries();
    Int_t nentries1 = (Int_t) chain1->GetEntries();

    cout << "(" << nentries0 << "," << nentries1 << ")" << endl << endl << endl << endl;

    vector <vector<float>> Check_ROC_S;
    vector <vector<float>> Check_ROC_B;
    vector <vector<float>> Check_ROC_T;
    for (Int_t i = 0; i < nentries0; i++) {
        chain0->GetEntry(i);
        vector<float> Single_jet;
        Single_jet.push_back(jetevaluated[4]);
        Single_jet.push_back(1);
        Single_jet.push_back(jetevaluated[2]);
        Single_jet.push_back(jetevaluated[3]);
        Check_ROC_S.push_back(Single_jet);
        Check_ROC_T.push_back(Single_jet);
    }

    for (Int_t i = 0; i < nentries1; i++) {
        chain1->GetEntry(i);
        vector<float> Single_jet;
        Single_jet.push_back(jetevaluated[4]);
        Single_jet.push_back(0);
        Single_jet.push_back(jetevaluated[2]);
        Single_jet.push_back(jetevaluated[3]);
        Check_ROC_B.push_back(Single_jet);
        Check_ROC_T.push_back(Single_jet);
    }


    sort(Check_ROC_S.begin(), Check_ROC_S.end(), sortcol);
    sort(Check_ROC_B.begin(), Check_ROC_B.end(), sortcol);
    sort(Check_ROC_T.begin(), Check_ROC_T.end(), sortcol);

    //time

    /*
    auto start = system_clock::now();

    auto end   = system_clock::now();
    auto duration = duration_cast<microseconds>(end - start);
    cout<< double(duration.count()) * microseconds::period::num / microseconds::period::den<< endl;

     */

    //time

    tuple < vector < vector < float >> , int, float > Result_tuple =ROC_Points(Check_ROC_S,Check_ROC_B,1000);

    cout<<"AUC Score: "<<get<2>(Result_tuple)<<endl<<endl;

    auto end = system_clock::now();
    auto duration = duration_cast<microseconds>(end - start);
    cout <<"============================"<<endl;
    cout <<"====Time Used: "<< double(duration.count()) * microseconds::period::num / microseconds::period::den <<"====="<<endl;
    //cout << double(duration.count()) * microseconds::period::num / microseconds::period::den << endl;
    cout <<"============================"<<endl;
    return_current_time_and_date();







    if(Output_or_not==1){
        return Result_tuple;

    }
    tuple < vector < vector < float >> , int, float > fake_output;
    vector < vector < float >> fake_vector;
    cout <<"============================"<<endl;
    cout <<"=======Result Hidden========"<<endl;
    return make_tuple(fake_vector,0,0);
}