# Jet Vertex Tagger 

This is a tool for separating hard-scattering and pile-up jet. A summary slides is contained in the folder Jet-Vertex-Tagger-Retraining/Summary_Report/


# Download the package

Fork  [Jet-Vertex-Tagger-Retraining-Models](https://gitlab.cern.ch/jxiang/jet-vertex-tagger-retraining-models)  on GitLab

Then run

```
git clone ssh://git@gitlab.cern.ch:7999/[username]/jet-vertex-tagger-retraining-models.git
```


## Retrieve data from DAOD file

A sample retrieved data is located in Jet-Vertex-Tagger-Retraining/Training_Code/DNN_Track/Dynet_ana/Root_Data/Root_Data_Calibrated.

You could retrieve the required information from a DAOD file by yourself, or using the sample code in Publish_Jet_Track_Analysis/ folder



Login lxplus6

Go to Jet-Vertex-Tagger-Retraining/Publish_Jet_Track_Analysis/ folder

Then run

```
setupATLAS
mkdir build
mkdir run
cd build 
asetup 21.2.87,AnalysisBase
source */setup.sh
cmake ../source/
make
cd ..
voms-proxy-init -voms atlas  (Password is required)
lsetup rucio
lsetup panda
source ./source/RunGrid
```
After the Grid finish all jobs, an email will be sent to your CERN mailbox. Follow the instruction to download the file from user.[username].test.361022.e3668_s3126_r9364_p3404_hist/


## Train the TBDNN Model
.
In a root6 environment

Go to Jet-Vertex-Tagger-Retraining/Check_ROC/ folder.

Go to the one of those folder named by the algorithm.

Modify the **TMVAClassification.C** on Line 

TString fname = "../DNN_Track/Dynet_ana/Root_Data/Root_Data_Calibrated/hist-mc16_13TeV.root";

Change the file path to the root file just downloaded by rucio.

Then run

```
root -l TMVAClassification.C

```

The Model will be saved in \dataset\weights folder.



## Use the Model

Please follow the instruction of Root TMVA.

Some example are stored in the folder.

For Example:

```
root -l Checketapt.cxx
```

A .json file and a .root file containing Test result will be created.